<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">

<title>Virtual Tour</title>

<!-- Fav Icon -->
<link rel="icon" href="assets/images/Lambang_Kota_Denpasar_(1).png" type="image/x-icon">

<!-- Google Fonts -->
<link href="https://fonts.googleapis.com/css2?family=Amatic+SC:wght@400;700&display=swap" rel="stylesheet">
<link href="https://fonts.googleapis.com/css2?family=Rubik:ital,wght@0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap" rel="stylesheet">

<!-- Stylesheets -->
<link href="assets/css/font-awesome-all.css" rel="stylesheet">
<link href="assets/css/flaticon.css" rel="stylesheet">
<link href="assets/css/owl.css" rel="stylesheet">
<link href="assets/css/bootstrap.css" rel="stylesheet">
<link href="assets/css/jquery.fancybox.min.css" rel="stylesheet">
<link href="assets/css/animate.css" rel="stylesheet">
<link href="assets/css/color.css" rel="stylesheet">
<link href="assets/css/style.css?v=2" rel="stylesheet">
<link href="assets/css/responsive.css" rel="stylesheet">

</head>


<!-- page wrapper -->
<body>

    <div class="boxed_wrapper">


        <!-- preloader -->
        <div class="preloader">
            <div class="boxes">
                <div class="box">
                    <div></div>
                    <div></div>
                    <div></div>
                    <div></div>
                </div>
                <div class="box">
                    <div></div>
                    <div></div>
                    <div></div>
                    <div></div>
                </div>
                <div class="box">
                    <div></div>
                    <div></div>
                    <div></div>
                    <div></div>
                </div>
                <div class="box">
                    <div></div>
                    <div></div>
                    <div></div>
                    <div></div>
                </div>
            </div>
        </div>
        <!-- preloader end -->


        <!-- main header -->
        <header class="main-header header-style-two" id="home">
            <div class="header-lower">
                <div class="container outer-box">
                    <div class="logo-box">
                        <figure class=""><a href="index.html"><img style="height: 80px; width: 80px;" src="assets/images/Lambang_Kota_Denpasar_(1).png" alt=""></a></figure>
                    </div>
                    <div class="menu-area">
                        <!--Mobile Navigation Toggler-->
                        <div class="mobile-nav-toggler">
                            <i class="icon-bar"></i>
                            <i class="icon-bar"></i>
                            <i class="icon-bar"></i>
                        </div>
                        <nav class="main-menu navbar-expand-md navbar-light">
                            <div class="collapse navbar-collapse show clearfix" id="navbarSupportedContent">
                                <ul class="navigation clearfix">
                                    <li class="current dropdown"><a href="#home">Beranda</a>
                                        
                                    </li> 
                                    <li class="dropdown"><a href="#tentang">Tentang</a>

                                    </li>
                                    <li class="dropdown"><a href="#galeri">Galeri</a>
                                       
                                    </li>
                                    <li class="dropdown"><a href="#virtual">Jelajah Virtual</a>
                                    </li>
                                </ul>
                            </div>
                        </nav>
                    </div>
                </div>
            </div>

            <!--sticky Header-->
            <div class="sticky-header">
                <div class="outer-box">
                    <div class="menu-area">
                        <nav class="main-menu clearfix">
                            <!--Keep This Empty / Menu will come through Javascript-->
                        </nav>
                    </div>
                </div>
            </div>
        </header>
        <!-- main-header end -->

        <!-- Mobile Menu  -->
        <div class="mobile-menu">
            <div class="menu-backdrop"></div>
            <div class="close-btn"><i class="fas fa-times"></i></div>
            
            <nav class="menu-box">
                <div class="nav-logo"><a href="index.html"><img style="height: 80px; width: 80px;" src="assets/images/Lambang_Kota_Denpasar_(1).png" alt="" title=""></a></div>
                <div class="menu-outer"><!--Here Menu Will Come Automatically Via Javascript / Same Menu as in Header--></div>
            </nav>
        </div><!-- End Mobile Menu -->


        <!-- banner-section -->
        <section class="banner-section style-two">
            <div class="pattern-layer" style="background-image: url(assets/images/shape/shape-1.png);"></div>
            <div class="banner-carousel owl-theme owl-carousel owl-dots-none">
                <div class="slide-item">
                    <div class="image-layer" style="background-image:url(assets/images/Bale\ Agung.jpg)"></div>
                    <div class="auto-container">
                        <div class="content-box">
                            <!-- <h2>Bale Agung</h2>
                            <span>Bale Agung terletak di Madya Mandala</span> -->
                            <h2>Virtual Tour</h2>
                            <span>Selamat datang dalam pengalaman virtual tour</span>
                        </div>  
                    </div>
                </div>
                <div class="slide-item style-two">
                    <div class="image-layer" style="background-image:url(assets/images/Tajuk-Susunan.jpg)"></div>
                    <div class="auto-container">
                        <div class="content-box">
                            <!-- <h2>Tajuk Susunan</h2>
                            <span>Tajuk Susunan atau Piyasan Tajuk Susunan terletak pada Utama Mandala</span> -->
                            <h2>Pura Desa Lan Puseh</h2>
                            <span>Desa Adat Denpasar</span> 
                        </div>  
                    </div>
                </div>
            </div>
        </section>
        <!-- banner-section end -->

        <!-- banner kunjungan -->
        <section class="activities-section sec-pad bg-color-1">
            <div class="auto-container" style="position: absolute;z-index: 2;top: -30%;left: 50%;transform: translate(-50%, -50%);width: 100%;">
                <div class="lower-box" style="border-radius: 10px; ">
                    <div class="sec-title light text-center col-lg-4">
                        <h3 style="color: #ffbf37; font-size: 20px;">Jam <span id="digital-clock"></span></h3>
                    </div>
                    <div class="sec-title light text-center col-lg-4 ">
                        <h3 class="py-3" style="color: white;">Total Kunjungan</h3>
                        <h3 id="access_count" style="color: #ffbf37;">0</h3>
                    </div>
                    <div class="sec-title light text-center col-lg-4">
                        <h3 id="date-display" style="font-size: 20px;"></h3>
                    </div>
                </div>
            </div>
            <img src="assets/images/barong.png" style="
    position: absolute;
    top: -60%;
    left: 13%;
    z-index: 5;
    width: 180px;
    transform: rotate(-45deg);
" alt="">
            <img src="assets/images/pura.png" style="
    position: absolute;
    top: -60%;
    right: 13%;
    z-index: 5;
    width: 180px;
    /* transform: rotate(-45deg); */
" alt="">
        </section>
        <!-- banner kunjungan end-->

        <!-- about-section -->
        <section class="about-section bg-color-1" id="tentang">
            <div class="pattern-layer" style="background-image: url(assets/images/shape/shape-4.png);"></div>
            <div class="auto-container">
                <div class="row clearfix">
                    <div class="col-lg-6 col-md-12 col-sm-12 image-column">
                        <div class="image_block_one">
                            <div class="image-box">
                                <iframe src="https://www.youtube.com/embed/ZAKlAKLKow4?si=rd9uHhjiquFTMNqd" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-12 col-sm-12 content-column">
                        <div class="content_block_one">
                            <div class="content-box">
                                <div class="sec-title">
                                    <h2>Pura Desa Lan Puseh Desa Adat Denpasar</h2>
                                </div>
                                <div class="text" style="text-align: justify;">
                                    <p>Pura Desa Lan Puseh Desa Adat Denpasar terletak di Jalan Gajah Mada, Dauh Puri Kangin, Kecamatan Denpasar Barat, Kota Denpasar, Provinsi Bali. Pura Desa Lan Puseh Desa Adat Denpasar berjarak 2,7 km dari pusat Kota Denpasar dan dapat ditempuh kurang lebih 6 menit.</p>
                                </div>
                                <div class="btn-box">
                                    <a href="halaman-tentang.html" class="theme-btn btn-one">Lihat Selengkapnya</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- about-section end -->

        <!-- gallery-style-two -->
        <section class="gallery-style-two" id="galeri">
            <div class="outer-container sec-pad">
                <div class="gallery-carousel owl-carousel owl-theme owl-nav-none owl-dots-none">
                    <div class="gallery-block-two">
                        <div class="inner-box">
                            <figure class="image"><img src="assets/images/galeri/galeri-6.jpg" alt=""></figure>
                            <div class="view-btn"><a href="assets/images/galeri/galeri-6.jpg" class="lightbox-image" data-fancybox="gallery"><i class="flaticon-plus"></i></a></div>
                        </div>
                    </div>
                    <div class="gallery-block-two">
                        <div class="inner-box">
                            <figure class="image"><img src="assets/images/galeri/galeri-2.jpg" alt=""></figure>
                            <div class="view-btn"><a href="assets/images/galeri/galeri-2.jpg" class="lightbox-image" data-fancybox="gallery"><i class="flaticon-plus"></i></a></div>
                        </div>
                    </div>
                    <div class="gallery-block-two">
                        <div class="inner-box">
                            <figure class="image"><img src="assets/images/galeri/galeri-3.jpg" alt=""></figure>
                            <div class="view-btn"><a href="assets/images/galeri/galeri-3.jpg" class="lightbox-image" data-fancybox="gallery"><i class="flaticon-plus"></i></a></div>
                        </div>
                    </div>
                    <div class="gallery-block-two">
                        <div class="inner-box">
                            <figure class="image"><img src="assets/images/galeri/galeri-7.jpg" alt=""></figure>
                            <div class="view-btn"><a href="assets/images/galeri/galeri-7.jpg" class="lightbox-image" data-fancybox="gallery"><i class="flaticon-plus"></i></a></div>
                        </div>
                    </div>
                    <div class="gallery-block-two">
                        <div class="inner-box">
                            <figure class="image"><img src="assets/images/galeri/galeri-5.jpg" alt=""></figure>
                            <div class="view-btn"><a href="assets/images/galeri/galeri-5.jpg" class="lightbox-image" data-fancybox="gallery"><i class="flaticon-plus"></i></a></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="btn-box text-center mb-5" id="virtual">
                <a href="galeri.html" class="theme-btn btn-one">Lihat Selengkapnya</a>
            </div>
        </section>
        <!-- gallery-style-two end -->

        <!-- video-section -->
        <section class="video-section mt-5">
            <div class="bg-layer parallax-bg" data-parallax='{"y": 100}' style="background-image: url(assets/images/gambar-virtual.jpg);"></div>
            <div class="auto-container">
                <div class="inner-box">
                    <div class="sec-title light">
                        <h2>Ayo Coba Jelajahi Virtual</h2>
                    </div>
                    <div class="video-btn">
                        <a href="https://virtualtour.treesistem.com/ambara/tour/" style="background-image: url(assets/images/shape/shape-7.png);"><i class="fas fa-play"></i></a>
                    </div>
                </div>
            </div>
        </section>
        <!-- video-section end -->

        <!-- vitrual-section -->
        <section class="about-section bg-color-1">
            <div class="pattern-layer" style="background-color: white;"></div>
            <div class="auto-container">
                <div class="row clearfix">
                    <div class="col-md-12 col-sm-12 content-column">
                        <div class="content_block_one">
                            <div class="content-box">
                                <div class="sec-title">
                                    <h2>Apa Itu Jelajah Virtual?</h2>
                                </div>
                                <div class="text" style="text-align: justify;">
                                    <p>Virtual tour atau jelajah virtual merupakan teknologi yang menempatkan user di dalam gambar dan memungkinkan user untuk meningkatkan kesadaran situasional serta meningkatkan daya lihat, tangkap dan menganalisa data virtual secara signifikan. Virtual tour merupakan sebuah simulasi dari sebuah lokasi yang terdiri dari rentetan. Rentetan gambar tersebut akan digabungkan (stitch) untuk menghasilkan foto panorama 360 derajat. Virtual tour sendiri biasanya digunakan untuk memberi pengalaman ‘pernah berada’ di suatu tempat hanya dengan melihat layar monitor. Penyajian virtual tour dapat dilakukan dengan cara memanfaatkan gambar ataupun video, selain itu dapat menggunakan model 3 dimensi. Untuk penyajian dengan menggunakan gambar, dapat digunakan foto panorama. Jenis foto panorama juga mempengaruhi hasil virtual tour yang dihasilkan. Untuk panorama jenis cylindrical, bagian vertikalnya hanya dapat menangkap tidak lebih dari 180 sedangkan jenis spherical, memungkinkan untuk melihat ke atas dan ke bawah</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- virtual-section end -->

        <!-- google-map-section -->
        <section class="google-map-section">
            <div class="shape" style="background-image: url(assets/images/shape/shape-9.png);"></div>
            <div class="map-inner">
                <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d63111.41994873078!2d115.12240541917399!3d-8.647366789157587!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2dd24098d6351609%3A0xafe7f7c9f5c52973!2zUHVyYSBEZXNhIGxhbiBQdXNlaCBEZXNhIEFkYXQgRGVucGFzYXIg4ayn4ay44ayt4ayk4ay-4ayw4ayu4aym4a2E4ayn4ay44ayy4a2C4ayE4ayk4ay-4ayw4ayF4ayk4ayi4a2E4ayk4ay-4aym4a2E4ayn4ayy4ayD4a2f!5e0!3m2!1sid!2sid!4v1695307986096!5m2!1sid!2sid" width="100%" height="600" style="border:0;" allowfullscreen="" loading="lazy" referrerpolicy="no-referrer-when-downgrade"></iframe>
            </div>
        </section>
        <!-- google-map-section end -->


        <!-- main-footer -->
        <footer class="main-footer pt-0">
            <div class="auto-container">
                
                <div class="widget-section">
                    <div class="row clearfix">
                        <div class="col-lg-4 col-sm-12 footer-column">
                            <div class="footer-widget links-widget">
                                <div class="widget-title">
                                    <h4>Navigasi</h4>
                                </div>
                                <div class="widget-content">
                                    <ul class="links-list clearfix">
                                        <li><a href="#home">Beranda</a></li>
                                        <li><a href="#tentang">Tentang</a></li>
                                        <li><a href="#galeri">Galeri</a></li>
                                        <li><a href="#virtual">Jelajah Virtual</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 col-sm-12 footer-column">
                            <div class="footer-widget contact-widget">
                                <div class="widget-title">
                                    <h4>Kontak</h4>
                                </div>
                                <div class="widget-content" style="text-align: justify;">
                                    <p>Jl. Gajah Mada No.22, Dauh Puri Kangin, Kec. Denpasar Utara, Kota Denpasar, Bali 80232</p>
                                    <ul class="social-links clearfix">
                                        <li><a href="https://api.whatsapp.com/send/?phone=6285155026771&text&type=phone_number&app_absent=0"><i class="fab fa-whatsapp"></i></a></li>
                                        <li><a href="https://www.instagram.com/ambaraa___/"><i class="fab fa-instagram"></i></a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 col-sm-12 footer-column">
                            <div class="footer-widget newsletter-widget">
                                <div class="widget-title">
                                    <h4>Terima kasih</h4>
                                </div>
                                <div class="widget-content" style="text-align: justify;">
                                    <p>Terima kasih telah mengunjungi halaman web ini. Kami sangat menghargai kunjungan Anda dan harap Anda menemukan informasi yang berguna.</p>
                                    <form action="contact.html" method="post" class="newsletter-form">
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </footer>
        <!-- main-footer end -->


        <!-- scroll to top -->
        <a class="scroll-top scroll-to-target" href="#home">
            <i class="fal fa-long-arrow-up"></i>
        </a>
    </div>


    <!-- jequery plugins -->
    <script src="assets/js/jquery.js"></script>
    <script src="assets/js/popper.min.js"></script>
    <script src="assets/js/bootstrap.min.js"></script>
    <script src="assets/js/owl.js"></script>
    <script src="assets/js/wow.js"></script>
    <script src="assets/js/validation.js"></script>
    <script src="assets/js/jquery.fancybox.js"></script>
    <script src="assets/js/appear.js"></script>
    <script src="assets/js/scrollbar.js"></script>
    <script src="assets/js/parallax-scroll.js"></script>
    <script src="assets/js/isotope.js"></script>
    <script src="assets/js/circle-progress.js"></script>
    <script src="assets/js/jquery.countTo.js"></script>

    <!-- map script -->
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyA-CE0deH3Jhj6GN4YvdCFZS7DpbXexzGU"></script>
    <script src="assets/js/gmaps.js"></script>
    <script src="assets/js/map-helper.js"></script>

    <!-- main-js -->
    <script src="assets/js/script.js?v=2"></script>
    <script>
        $(document).ready(function () {
            $.ajax({
                type: "GET",
                url: './src/access_count.php',
                success: function (response) {
                    $('#access_count').html(response);
                }
            });
        });
    </script>
    <!-- jam -->
        <script>
    function updateClock() {
      var currentTime = new Date();
      var hours = currentTime.getHours();
      var minutes = currentTime.getMinutes();
      var seconds = currentTime.getSeconds();

      // Menambahkan nol di depan jika angka kurang dari 10
      hours = (hours < 10) ? "0" + hours : hours;
      minutes = (minutes < 10) ? "0" + minutes : minutes;
      seconds = (seconds < 10) ? "0" + seconds : seconds;

      var timeString = hours + ":" + minutes + ":" + seconds;
      document.getElementById("digital-clock").innerText = timeString;
    }

    // Memperbarui jam setiap detik
    setInterval(updateClock, 1000);

    // Memanggil fungsi untuk pertama kali
    updateClock();
  </script>

  <!-- dd/mm/yyyy -->
  <script>
    function updateDate() {
      var currentDate = new Date();
      var day = currentDate.getDate();
      var month = currentDate.toLocaleString('default', { month: 'long' });
      var year = currentDate.getFullYear();

      // Menambahkan nol di depan jika angka kurang dari 10
      day = (day < 10) ? "0" + day : day;

      var dateString = day + " " + month + " " + year;
      document.getElementById("date-display").innerText = dateString;
    }

    // Memanggil fungsi untuk pertama kali
    updateDate();
  </script>
</body><!-- End of .page_wrapper -->
</html>
