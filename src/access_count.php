<?php 
    header("Access-Control-Allow-Origin: *");
    header("Content-Type: application/json");
    $servername = "localhost";
    $username = "root";
    $password = "";
    $database = "virtual_tour";
    
    $conn = new mysqli($servername, $username, $password, $database);
    $data = $conn->query("SELECT * FROM access_count WHERE access_count.id = 1");
    $result = $data->fetch_assoc();
    $count = $result['count'] + 1;
    $update = $conn->query("UPDATE access_count SET count='".htmlspecialchars($count)."' WHERE id=1");
    echo json_encode($count);
    
?>