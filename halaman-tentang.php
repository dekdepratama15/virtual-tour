<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">

<title>Virtual Tour || About</title>

<!-- Fav Icon -->
<link rel="icon" href="assets/images/Lambang_Kota_Denpasar_(1).png" type="image/x-icon">

<!-- Google Fonts -->
<link href="https://fonts.googleapis.com/css2?family=Amatic+SC:wght@400;700&display=swap" rel="stylesheet">
<link href="https://fonts.googleapis.com/css2?family=Poppins:wght@300;400;500;600;700;800;900&display=swap" rel="stylesheet">

<!-- Stylesheets -->
<link href="assets/css/font-awesome-all.css" rel="stylesheet">
<link href="assets/css/flaticon.css" rel="stylesheet">
<link href="assets/css/owl.css" rel="stylesheet">
<link href="assets/css/bootstrap.css" rel="stylesheet">
<link href="assets/css/jquery.fancybox.min.css" rel="stylesheet">
<link href="assets/css/animate.css" rel="stylesheet">
<link href="assets/css/color.css" rel="stylesheet">
<link href="assets/css/style.css" rel="stylesheet">
<link href="assets/css/responsive.css" rel="stylesheet">

</head>


<!-- page wrapper -->
<body>

    <div class="boxed_wrapper">


        <!-- preloader -->
        <div class="preloader">
            <div class="boxes">
                <div class="box">
                    <div></div>
                    <div></div>
                    <div></div>
                    <div></div>
                </div>
                <div class="box">
                    <div></div>
                    <div></div>
                    <div></div>
                    <div></div>
                </div>
                <div class="box">
                    <div></div>
                    <div></div>
                    <div></div>
                    <div></div>
                </div>
                <div class="box">
                    <div></div>
                    <div></div>
                    <div></div>
                    <div></div>
                </div>
            </div>
        </div>
        <!-- preloader end -->


        <!-- main header -->
        <header class="main-header" id="home">
            <div class="header-lower">
                <div class="container outer-box">
                    <div class="logo-box">
                        <figure class=""><a href="index.html"><img style="height: 80px; width: 80px;" src="assets/images/Lambang_Kota_Denpasar_(1).png" alt=""></a></figure>
                    </div>
                    <div class="menu-area">
                        <!--Mobile Navigation Toggler-->
                        <div class="mobile-nav-toggler">
                            <i class="icon-bar"></i>
                            <i class="icon-bar"></i>
                            <i class="icon-bar"></i>
                        </div>
                        <nav class="main-menu navbar-expand-md navbar-light">
                            <div class="collapse navbar-collapse show clearfix" id="navbarSupportedContent">
                                <ul class="navigation clearfix">
                                    <li class="dropdown"><a href="index.html#home">Beranda</a>
                                        
                                    </li> 
                                    <li class="current dropdown"><a href="#tentang">Tentang</a>

                                    </li>
                                    <li class="dropdown"><a href="index.html#galeri">Galeri</a>
                                       
                                    </li>
                                    <li class="dropdown"><a href="index.html#virtual">Jelajah Virtual</a>
                                    </li>
                                </ul>
                            </div>
                        </nav>
                    </div>
                </div>
            </div>

            <!--sticky Header-->
            <div class="sticky-header">
                <div class="outer-box">
                    <div class="menu-area">
                        <nav class="main-menu clearfix">
                            <!--Keep This Empty / Menu will come through Javascript-->
                        </nav>
                    </div>
                </div>
            </div>
        </header>
        <!-- main-header end -->

        <!-- Mobile Menu  -->
        <div class="mobile-menu">
            <div class="menu-backdrop"></div>
            <div class="close-btn"><i class="fas fa-times"></i></div>
            
            <nav class="menu-box">
                <div class="nav-logo"><a href="index.html"><img style="height: 80px; width: 80px;" src="assets/images/Lambang_Kota_Denpasar_(1).png" alt="" title=""></a></div>
                <div class="menu-outer"><!--Here Menu Will Come Automatically Via Javascript / Same Menu as in Header--></div>
            </nav>
        </div><!-- End Mobile Menu -->

        <!-- about-section -->
        <section class="about-section bg-color-1" id="tentang">
            <div class="pattern-layer" style="background-image: url(assets/images/shape/shape-4.png);"></div>
            <div class="auto-container">
                <div class="row clearfix">
                    <div class="col-lg-6 col-md-12 col-sm-12 image-column">
                        <div class="image_block_one">
                            <div class="image-box">
                                <div class="shape" style="background-image: url(assets/images/shape/shape-3.png);"></div>
                                <figure class="image"><img src="assets/images/about-images.jpg" alt=""></figure>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-12 col-sm-12 content-column">
                        <div class="content_block_one">
                            <div class="content-box">
                                <div class="sec-title">
                                    <h2>Pura Desa Lan Puseh Desa Adat Denpasar</h2>
                                </div>
                                <div class="text" style="text-align: justify;">
                                    <p>Pura Desa Lan Puseh Desa Adat Denpasar terletak di Jalan Gajah Mada, Dauh Puri Kangin, Kecamatan Denpasar Barat, Kota Denpasar, Provinsi Bali. Pura Desa Lan Puseh Desa Adat Denpasar berjarak 2,7 km dari pusat Kota Denpasar dan dapat ditempuh kurang lebih 6 menit.</p>
                                    <p>Keberadaan Pura Desa Lan Puseh Desa Adat Denpasar sekilas termuat dalam "Monografi Desa Adat Denpasar", yaitu ketika pemerintahan Ida Cokorda Ngurah Made Pemecutan di Puri Denpasar pada tahun 1870. Raja pada tahun itu dijelaskan melakukan penataan kembali tata pemerintahan dan keagamaan dengan menetapkan Pileket Bumi Badung  tentang galungan, tumpek landep, ngusaba nini, palebon,
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-12 col-sm-12 content-column">
                        <div class="content_block_one">
                            <div class="content-box">
                                <div class="text" style="text-align: justify;">
                                    <p>ngerebeg, dan yang terpenting adalah pembagian pangempon Pura Kahyangan Tiga, yaitu Pura Dalem Kahyangan Badung diawasi oleh Puri Pemecutan dan Pura Puseh Desa diawasi oleh Puri Denpasar. Kemudian mengenai pemangku yang bertugas di Pura Pura Desa lan Puseh Desa Adat Denpasar dari Banjar Wangaya Kelod, maka dengan itu Banjar Wangaya Kelod dan masyarakatnya ditetapkan sebagai pengempon ngarep Pura Desa Lan Puseh Desa Adat Denpasar.</p>
                                    <p>Menurut penuturan pemangku Pura Pedarman Satriya Denpasar, yaitu Anak Agung Ngurah Mayun, sebelum berdirinya Pura Desa Lan Puseh Desa Adat Denpasar seperti sekarang, di lokasi ini yang paling pertama dibangun adalah Palinggih Ida Ratu Hyang Agung oleh Keturunan Ki Bandesa Manik Mas, yaitu Ki Gede Bandesa ketika mengiringi Kyai Notor Wandira sampai di tanah badeng (Badung) dari Batur Songan. Selain membangun palinggih, ditanam juga panca datu dan </p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-12 col-sm-12 image-column">
                        <div class="image_block_one">
                            <div class="image-box">
                                <div class="shape" style="background-image: url(assets/images/shape/shape-3.png);"></div>
                                <figure class="image"><img src="assets/images/galeri/galeri-11.jpg" alt=""></figure>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-12 col-sm-12 image-column">
                        <div class="image_block_one">
                            <div class="image-box">
                                <div class="shape" style="background-image: url(assets/images/shape/shape-3.png);"></div>
                                <figure class="image"><img src="assets/images/galeri/gambar-16.png" alt=""></figure>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-12 col-sm-12 content-column">
                        <div class="content_block_one">
                            <div class="content-box">
                                <div class="text" style="text-align: justify;">
                                    <p>pohon pule di timur laut pura, tepatnya di belakang Palinggih Ida Ratu Hyang Agung dan kemudian lokasi ini disebut dengan Pura Gaduh. Hingga saat ini cerita tersebut tetap diwarisi secara turun temurun, ditandai dengan pemangku gede Pura Desa Lan Puseh Desa Adat Denpasar tetap berasal dari keturunan Ki Gede Bandesa di Banjar Wangaya Kelod.
                                        Cerita mengenai Ki Gede Bandesa sebagai tokoh penting pada masanya, seperti menjadi pengiring (pengikut) setia Kyai Notor Wandira ketika mendirikan Kerajaan Badung dan hingga mendirikan Palinggih Ida Ratu Hyang Ibu di lokasi Pura Desa Lan Puseh Desa Adat Denpasar didukung dengan temuan prasasti berangka tahun Saka 1445 (1523 Masehi). Prasasti tersebut disimpan Oleh Keluarga keturunan Ki Gede Bandesa di Pura Dadia Bandesa Wangaya Kelod, Kadang juga pada waktu-waktu tertentu diusung ke Pura Desa Lan Puseh Desa Adat Denpasar untuk diupacarai. Prasasti memuat tentang aturan yang harus dilaksanakan oleh seluruh warga dari berbagai lapisan yang tinggal di wilayah Badung, perihal</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-12 col-sm-12 content-column">
                        <div class="content_block_one">
                            <div class="content-box">
                                <div class="text" style="text-align: justify;">
                                    <p>membayar kewajiban utang piutang, aturan gadai menggadai, sawah pelaba (penahas) harus mengacu pada aturan, sistem membayar pajak (pasak), aturan menebang pohon di pekarangan, hutan dan gunung. Hal yang terpenting dalam prasasti ini adalah penyebutan batas-batas wilayah Badung, seperti Pamogan Rangdhu, Pakicek, Padhang Luwah, Lepang, dan Padhang Mugged. Dalam prasasti ini disebutkan juga nama besar Patih Gajah Mada dan Kerajaan Majapahit pada tahun Saka 1291 (1469 Masehi) dengan pejabat Adipati Sang Arya Gajahpati yang didampingi oleh Patih Wwluhwuh Bakyat.</p>
                                    <p>Kemudian sekitar tahun Saka 1825 Pura Desa Lan Puseh Desa Adat Denpasar yang semula terletak di selatan jalan (sekarang areal parkir Pasar Badung), dipindahkan dan dijadikan satu dengan Pura Gaduh. Proses pembangunan terus berlangsung hingga tahun Saka 1870 ditandai dengan selesainya pengerjaan arca dwarapala termasuk Kori Agung. Angka 1870 sebagai tahun Saka terpahat di belakang lapik</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-12 col-sm-12 image-column">
                        <div class="image_block_one">
                            <div class="image-box">
                                <div class="shape" style="background-image: url(assets/images/shape/shape-3.png);"></div>
                                <figure class="image"><img src="assets/images/galeri/gambar-17.png" alt=""></figure>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-12 col-sm-12 image-column">
                        <div class="image_block_one">
                            <div class="image-box">
                                <div class="shape" style="background-image: url(assets/images/shape/shape-3.png);"></div>
                                <figure class="image"><img src="assets/images/galeri/galeri12.jpg" alt=""></figure>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-12 col-sm-12 content-column">
                        <div class="content_block_one">
                            <div class="content-box">
                                <div class="text" style="text-align: justify;">
                                    <p>arca dwarapala di depan Kori Agung pura. Setelah Pura Desa Lan Puseh Desa Adat Denpasar dipindahkan ke utara, dilanjutkan dengan memindahkan Pura Puseh  yang dulu terletak di wilayah Banjar Jematang, sehingga Pura Puseh Desa lan Bale Agung Denpasar secara struktur menjadi satu di lokasi yang sekarang. Sedangkan Pura Puseh yang lama di Banjar Jumatang tetap dipertahankan hingga sekarang, karena pemangku dan masyarakat penyungsung masih  tetap ada dari Banjar Jematang.
                                        Pura Desa Lan Puseh Desa Adat Denpasar secara karakter dapat digolongkan sebagai pura territorial, karena memiliki ciri kesatuan wilayah (territorial) sebagai tempat pemujaan dari anggota masyarakat suatu desa yang diikat oleh kesatuan wilayah, kesatuan wilayah desa tersebut adalah Desa Adat Denpasar yang diikat dengan Kahyangana tiga, yaitu Pura Desa lan Bale Agung, Pura Puseh, dan Pura Dalem. Upacara piodalan dilaksanakan setiap enam bulan sekali, yaitu pada hari Selasa Kliwon (Anggarkasih) Wuku Tambir.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-12 col-sm-12 content-column">
                        <div class="content_block_one">
                            <div class="content-box">
                                <div class="text" style="text-align: justify;">
                                        <p>Penyungsung Pura Desa Lan Puseh Desa Adat Denpasar adalah seluruh masyarakat Desa Adat denpasar yang terhimpun di 105 banjar adat, tetapi dikelola atau pangempon yang bertanggung jawab mengurus dan membina keberadaan pura ini adalah 18 banjar-banjar adat panyibeh, yaitu Banjar Wangaya Kelod, Banjar Wangaya Kaja, Banjar Lumintang, Banjar Tampakgangsul, Banjar Tainsiat, Banjar Lelangon, Banjar Belaluan, Banjar Belaluan Sadmerta, Banjar Titih Kaja, Banjar Titih Tengah, Banjar Titih Kelod, Banjar Pekambingan, Banjar Suci, Banjar Alangkajeng Gede, Banjar Pemeregan, Banjar Grenceng, Banjar Panti Gede, Dan Banjar Belong.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-12 col-sm-12 image-column">
                        <div class="image_block_one">
                            <div class="image-box">
                                <div class="shape" style="background-image: url(assets/images/shape/shape-3.png);"></div>
                                <figure class="image"><img src="assets/images/galeri/galeri-2.jpg" alt=""></figure>
                            </div>
                        </div>
                    </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- about-section end -->


        <!-- main-footer -->
        <footer class="main-footer pt-0">
            <div class="auto-container">
                
                <div class="widget-section">
                    <div class="row clearfix">
                        <div class="col-lg-4 col-sm-12 footer-column">
                            <div class="footer-widget links-widget">
                                <div class="widget-title">
                                    <h4>Navigasi</h4>
                                </div>
                                <div class="widget-content">
                                    <ul class="links-list clearfix">
                                        <li><a href="index.html#home">Beranda</a></li>
                                        <li><a href="#tentang">Tentang</a></li>
                                        <li><a href="index.html#galeri">Galeri</a></li>
                                        <li><a href="index.html#virtual">Jelajah Virtual</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 col-sm-12 footer-column">
                            <div class="footer-widget contact-widget">
                                <div class="widget-title">
                                    <h4>Kontak</h4>
                                </div>
                                <div class="widget-content" style="text-align: justify;">
                                    <p>Jl. Gajah Mada No.22, Dauh Puri Kangin, Kec. Denpasar Utara, Kota Denpasar, Bali 80232</p>
                                    <ul class="social-links clearfix">
                                        <li><a href="https://api.whatsapp.com/send/?phone=6285155026771&text&type=phone_number&app_absent=0"><i class="fab fa-whatsapp"></i></a></li>
                                        <li><a href="https://www.instagram.com/ambaraa___"><i class="fab fa-instagram"></i></a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 col-sm-12 footer-column">
                            <div class="footer-widget newsletter-widget">
                                <div class="widget-title">
                                    <h4>Terima kasih</h4>
                                </div>
                                <div class="widget-content" style="text-align: justify;">
                                    <p>Terima kasih telah mengunjungi halaman web ini. Kami sangat menghargai kunjungan Anda dan harap Anda menemukan informasi yang berguna.</p>
                                    <form action="contact.html" method="post" class="newsletter-form">
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </footer>
        <!-- main-footer end -->


        <!-- scroll to top -->
        <a class="scroll-top scroll-to-target" href="#home">
            <i class="fal fa-long-arrow-up"></i>
        </a>
    </div>


    <!-- jequery plugins -->
    <script src="assets/js/jquery.js"></script>
    <script src="assets/js/popper.min.js"></script>
    <script src="assets/js/bootstrap.min.js"></script>
    <script src="assets/js/owl.js"></script>
    <script src="assets/js/wow.js"></script>
    <script src="assets/js/validation.js"></script>
    <script src="assets/js/jquery.fancybox.js"></script>
    <script src="assets/js/appear.js"></script>
    <script src="assets/js/scrollbar.js"></script>
    <script src="assets/js/parallax-scroll.js"></script>
    <script src="assets/js/isotope.js"></script>

    <!-- main-js -->
    <script src="assets/js/script.js"></script>

</body><!-- End of .page_wrapper -->
</html>
