<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">

<title>Virtual Tour || Galeri</title>

<!-- Fav Icon -->
<link rel="icon" href="assets/images/Lambang_Kota_Denpasar_(1).png" type="image/x-icon">

<!-- Google Fonts -->
<link href="https://fonts.googleapis.com/css2?family=Amatic+SC:wght@400;700&display=swap" rel="stylesheet">
<link href="https://fonts.googleapis.com/css2?family=Rubik:ital,wght@0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap" rel="stylesheet">

<!-- Stylesheets -->
<link href="assets/css/font-awesome-all.css" rel="stylesheet">
<link href="assets/css/flaticon.css" rel="stylesheet">
<link href="assets/css/owl.css" rel="stylesheet">
<link href="assets/css/bootstrap.css" rel="stylesheet">
<link href="assets/css/jquery.fancybox.min.css" rel="stylesheet">
<link href="assets/css/animate.css" rel="stylesheet">
<link href="assets/css/color.css" rel="stylesheet">
<link href="assets/css/style.css" rel="stylesheet">
<link href="assets/css/responsive.css" rel="stylesheet">

</head>


<!-- page wrapper -->
<body>

    <div class="boxed_wrapper">


        <!-- preloader -->
        <div class="preloader">
            <div class="boxes">
                <div class="box">
                    <div></div>
                    <div></div>
                    <div></div>
                    <div></div>
                </div>
                <div class="box">
                    <div></div>
                    <div></div>
                    <div></div>
                    <div></div>
                </div>
                <div class="box">
                    <div></div>
                    <div></div>
                    <div></div>
                    <div></div>
                </div>
                <div class="box">
                    <div></div>
                    <div></div>
                    <div></div>
                    <div></div>
                </div>
            </div>
        </div>
        <!-- preloader end -->


        <!-- main header -->
        <header class="main-header">
            <div class="header-lower">
                <div class="container outer-box">
                    <div class="logo-box">
                        <figure class=""><a href="index.html"><img style="height: 80px; width: 80px;" src="assets/images/Lambang_Kota_Denpasar_(1).png" alt=""></a></figure>
                    </div>
                    <div class="menu-area">
                        <!--Mobile Navigation Toggler-->
                        <div class="mobile-nav-toggler">
                            <i class="icon-bar"></i>
                            <i class="icon-bar"></i>
                            <i class="icon-bar"></i>
                        </div>
                        <nav class="main-menu navbar-expand-md navbar-light">
                            <div class="collapse navbar-collapse show clearfix" id="navbarSupportedContent">
                                <ul class="navigation clearfix">
                                    <li class="dropdown"><a href="index.html#home">Beranda</a>
                                        
                                    </li> 
                                    <li class="dropdown"><a href="index.html#tentang">Tentang</a>

                                    </li>
                                    <li class="current dropdown"><a href="#galeri">Galeri</a>
                                       
                                    </li>
                                    <li class="dropdown"><a href="index.html#virtual">Jelajah Virtual</a>
                                    </li>
                                </ul>
                            </div>
                        </nav>
                    </div>
                </div>
            </div>

            <!--sticky Header-->
            <div class="sticky-header">
                <div class="outer-box">
                    <div class="menu-area">
                        <nav class="main-menu clearfix">
                            <!--Keep This Empty / Menu will come through Javascript-->
                        </nav>
                    </div>
                </div>
            </div>
        </header>
        <!-- main-header end -->

        <!-- Mobile Menu  -->
        <div class="mobile-menu">
            <div class="menu-backdrop"></div>
            <div class="close-btn"><i class="fas fa-times"></i></div>
            
            <nav class="menu-box">
                <div class="nav-logo"><a href="index.html"><img style="height: 80px; width: 80px;" src="assets/images/Lambang_Kota_Denpasar_(1).png" alt="" title=""></a></div>
                <div class="menu-outer"><!--Here Menu Will Come Automatically Via Javascript / Same Menu as in Header--></div>
            </nav>
        </div><!-- End Mobile Menu -->

        <!-- gallery-page-section -->
        <section class="gallery-page-section" id="galeri">
            <div class="auto-container">
                <div class="sec-title text-center">
                    <h2>Galeri</h2>
                </div>
                <div class="row clearfix">
                    <div class="col-lg-4 col-md-6 col-sm-12 gallery-block">
                        <div class="gallery-block-two">
                            <div class="inner-box">
                                <figure class="image"><img src="assets/images/galeri=2/1.png" alt=""></figure>
                                <div class="view-btn"><a href="assets/images/galeri=2/1.png" class="lightbox-image" data-fancybox="gallery" data-song="song1"><i class="flaticon-plus"></i></a></div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6 col-sm-12 gallery-block">
                        <div class="gallery-block-two">
                            <div class="inner-box">
                                <figure class="image"><img src="assets/images/galeri=2/2.png" alt=""></figure>
                                <div class="view-btn"><a href="assets/images/galeri=2/2.png" class="lightbox-image" data-fancybox="gallery" data-song="song2"><i class="flaticon-plus"></i></a></div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6 col-sm-12 gallery-block">
                        <div class="gallery-block-two">
                            <div class="inner-box">
                                <figure class="image"><img src="assets/images/galeri=2/3.png" alt=""></figure>
                                <div class="view-btn"><a href="assets/images/galeri=2/3.png" class="lightbox-image" data-fancybox="gallery" data-song="song3"><i class="flaticon-plus"></i></a></div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6 col-sm-12 gallery-block">
                        <div class="gallery-block-two">
                            <div class="inner-box">
                                <figure class="image"><img src="assets/images/galeri=2/4.png" alt=""></figure>
                                <div class="view-btn"><a href="assets/images/galeri=2/4.png" class="lightbox-image" data-fancybox="gallery" data-song="song4"><i class="flaticon-plus"></i></a></div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6 col-sm-12 gallery-block">
                        <div class="gallery-block-two">
                            <div class="inner-box">
                                <figure class="image"><img src="assets/images/galeri=2/5.png" alt=""></figure>
                                <div class="view-btn"><a href="assets/images/galeri=2/5.png" class="lightbox-image" data-fancybox="gallery" data-song="song5"><i class="flaticon-plus"></i></a></div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6 col-sm-12 gallery-block">
                        <div class="gallery-block-two">
                            <div class="inner-box">
                                <figure class="image"><img src="assets/images/galeri=2/6.png" alt=""></figure>
                                <div class="view-btn"><a href="assets/images/galeri=2/6.png" class="lightbox-image" data-fancybox="gallery" data-song="song6"><i class="flaticon-plus"></i></a></div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6 col-sm-12 gallery-block">
                        <div class="gallery-block-two">
                            <div class="inner-box">
                                <figure class="image"><img src="assets/images/galeri=2/7.png" alt=""></figure>
                                <div class="view-btn"><a href="assets/images/galeri=2/7.png" class="lightbox-image" data-fancybox="gallery" data-song="song7"><i class="flaticon-plus"></i></a></div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6 col-sm-12 gallery-block">
                        <div class="gallery-block-two">
                            <div class="inner-box">
                                <figure class="image"><img src="assets/images/galeri=2/8.png" alt=""></figure>
                                <div class="view-btn"><a href="assets/images/galeri=2/8.png" class="lightbox-image" data-fancybox="gallery" data-song="song8"><i class="flaticon-plus"></i></a></div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6 col-sm-12 gallery-block">
                        <div class="gallery-block-two">
                            <div class="inner-box">
                                <figure class="image"><img src="assets/images/galeri=2/9.png" alt=""></figure>
                                <div class="view-btn"><a href="assets/images/galeri=2/9.png" class="lightbox-image" data-fancybox="gallery" data-song="song9"><i class="flaticon-plus"></i></a></div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6 col-sm-12 gallery-block">
                        <div class="gallery-block-two">
                            <div class="inner-box">
                                <figure class="image"><img src="assets/images/galeri=2/10.png" alt=""></figure>
                                <div class="view-btn"><a href="assets/images/galeri=2/10.png" class="lightbox-image" data-fancybox="gallery" data-song="song10"><i class="flaticon-plus"></i></a></div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6 col-sm-12 gallery-block">
                        <div class="gallery-block-two">
                            <div class="inner-box">
                                <figure class="image"><img src="assets/images/galeri=2/11.png" alt=""></figure>
                                <div class="view-btn"><a href="assets/images/galeri=2/11.png" class="lightbox-image" data-fancybox="gallery" data-song="song11"><i class="flaticon-plus"></i></a></div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6 col-sm-12 gallery-block">
                        <div class="gallery-block-two">
                            <div class="inner-box">
                                <figure class="image"><img src="assets/images/galeri=2/12.png" alt=""></figure>
                                <div class="view-btn"><a href="assets/images/galeri=2/12.png" class="lightbox-image" data-fancybox="gallery" data-song="song12"><i class="flaticon-plus"></i></a></div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6 col-sm-12 gallery-block">
                        <div class="gallery-block-two">
                            <div class="inner-box">
                                <figure class="image"><img src="assets/images/galeri=2/13.png" alt=""></figure>
                                <div class="view-btn"><a href="assets/images/galeri=2/13.png" class="lightbox-image" data-fancybox="gallery" data-song="song13"><i class="flaticon-plus"></i></a></div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6 col-sm-12 gallery-block">
                        <div class="gallery-block-two">
                            <div class="inner-box">
                                <figure class="image"><img src="assets/images/galeri=2/14.png" alt=""></figure>
                                <div class="view-btn"><a href="assets/images/galeri=2/14.png" class="lightbox-image" data-fancybox="gallery" data-song="song14"><i class="flaticon-plus"></i></a></div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6 col-sm-12 gallery-block">
                        <div class="gallery-block-two">
                            <div class="inner-box">
                                <figure class="image"><img src="assets/images/galeri=2/15.png" alt=""></figure>
                                <div class="view-btn"><a href="assets/images/galeri=2/15.png" class="lightbox-image" data-fancybox="gallery" data-song="song15"><i class="flaticon-plus"></i></a></div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6 col-sm-12 gallery-block">
                        <div class="gallery-block-two">
                            <div class="inner-box">
                                <figure class="image"><img src="assets/images/galeri=2/16.png" alt=""></figure>
                                <div class="view-btn"><a href="assets/images/galeri=2/16.png" class="lightbox-image" data-fancybox="gallery" data-song="song16"><i class="flaticon-plus"></i></a></div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6 col-sm-12 gallery-block">
                        <div class="gallery-block-two">
                            <div class="inner-box">
                                <figure class="image"><img src="assets/images/galeri=2/17.png" alt=""></figure>
                                <div class="view-btn"><a href="assets/images/galeri=2/17.png" class="lightbox-image" data-fancybox="gallery" data-song="song17"><i class="flaticon-plus"></i></a></div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6 col-sm-12 gallery-block">
                        <div class="gallery-block-two">
                            <div class="inner-box">
                                <figure class="image"><img src="assets/images/galeri=2/18.png" alt=""></figure>
                                <div class="view-btn"><a href="assets/images/galeri=2/18.png" class="lightbox-image" data-fancybox="gallery" data-song="song18"><i class="flaticon-plus"></i></a></div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6 col-sm-12 gallery-block">
                        <div class="gallery-block-two">
                            <div class="inner-box">
                                <figure class="image"><img src="assets/images/galeri=2/19.png" alt=""></figure>
                                <div class="view-btn"><a href="assets/images/galeri=2/19.png" class="lightbox-image" data-fancybox="gallery" data-song="song19"><i class="flaticon-plus"></i></a></div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6 col-sm-12 gallery-block">
                        <div class="gallery-block-two">
                            <div class="inner-box">
                                <figure class="image"><img src="assets/images/galeri=2/20.png" alt=""></figure>
                                <div class="view-btn"><a href="assets/images/galeri=2/20.png" class="lightbox-image" data-fancybox="gallery"  data-song="song20"><i class="flaticon-plus"></i></a></div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6 col-sm-12 gallery-block">
                        <div class="gallery-block-two">
                            <div class="inner-box">
                                <figure class="image"><img src="assets/images/galeri=2/21.png" alt=""></figure>
                                <div class="view-btn"><a href="assets/images/galeri=2/21.png" class="lightbox-image" data-fancybox="gallery" data-song="song21"><i class="flaticon-plus"></i></a></div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6 col-sm-12 gallery-block">
                        <div class="gallery-block-two">
                            <div class="inner-box">
                                <figure class="image"><img src="assets/images/galeri=2/22.png" alt=""></figure>
                                <div class="view-btn"><a href="assets/images/galeri=2/22.png" class="lightbox-image" data-fancybox="gallery" data-song="song22"><i class="flaticon-plus"></i></a></div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6 col-sm-12 gallery-block">
                        <div class="gallery-block-two">
                            <div class="inner-box">
                                <figure class="image"><img src="assets/images/galeri=2/23.png" alt=""></figure>
                                <div class="view-btn"><a href="assets/images/galeri=2/23.png" class="lightbox-image" data-fancybox="gallery" data-song="song23"><i class="flaticon-plus"></i></a></div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6 col-sm-12 gallery-block">
                        <div class="gallery-block-two">
                            <div class="inner-box">
                                <figure class="image"><img src="assets/images/galeri=2/24.png" alt=""></figure>
                                <div class="view-btn"><a href="assets/images/galeri=2/24.png" class="lightbox-image" data-fancybox="gallery" data-song="song24"><i class="flaticon-plus"></i></a></div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6 col-sm-12 gallery-block">
                        <div class="gallery-block-two">
                            <div class="inner-box">
                                <figure class="image"><img src="assets/images/galeri=2/25.png" alt=""></figure>
                                <div class="view-btn"><a href="assets/images/galeri=2/25.png" class="lightbox-image" data-fancybox="gallery" data-song="song25"><i class="flaticon-plus"></i></a></div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6 col-sm-12 gallery-block">
                        <div class="gallery-block-two">
                            <div class="inner-box">
                                <figure class="image"><img src="assets/images/galeri=2/26.png" alt=""></figure>
                                <div class="view-btn"><a href="assets/images/galeri=2/26.png" class="lightbox-image" data-fancybox="gallery" data-song="song26"><i class="flaticon-plus"></i></a></div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6 col-sm-12 gallery-block">
                        <div class="gallery-block-two">
                            <div class="inner-box">
                                <figure class="image"><img src="assets/images/galeri=2/28.png" alt=""></figure>
                                <div class="view-btn"><a href="assets/images/galeri=2/28.png" class="lightbox-image" data-fancybox="gallery" data-song="song27"><i class="flaticon-plus"></i></a></div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6 col-sm-12 gallery-block">
                        <div class="gallery-block-two">
                            <div class="inner-box">
                                <figure class="image"><img src="assets/images/galeri=2/29.png" alt=""></figure>
                                <div class="view-btn"><a href="assets/images/galeri=2/29.png" class="lightbox-image" data-fancybox="gallery" data-song="song28"><i class="flaticon-plus"></i></a></div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6 col-sm-12 gallery-block">
                        <div class="gallery-block-two">
                            <div class="inner-box">
                                <figure class="image"><img src="assets/images/galeri=2/30.png" alt=""></figure>
                                <div class="view-btn"><a href="assets/images/galeri=2/30.png" class="lightbox-image" data-fancybox="gallery" data-song="song29"><i class="flaticon-plus"></i></a></div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6 col-sm-12 gallery-block">
                        <div class="gallery-block-two">
                            <div class="inner-box">
                                <figure class="image"><img src="assets/images/galeri=2/31.png" alt=""></figure>
                                <div class="view-btn"><a href="assets/images/galeri=2/31.png" class="lightbox-image" data-fancybox="gallery" data-song="song30"><i class="flaticon-plus"></i></a></div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6 col-sm-12 gallery-block">
                        <div class="gallery-block-two">
                            <div class="inner-box">
                                <figure class="image"><img src="assets/images/galeri=2/32.png" alt=""></figure>
                                <div class="view-btn"><a href="assets/images/galeri=2/32.png" class="lightbox-image" data-fancybox="gallery" data-song="song31"><i class="flaticon-plus"></i></a></div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6 col-sm-12 gallery-block">
                        <div class="gallery-block-two">
                            <div class="inner-box">
                                <figure class="image"><img src="assets/images/galeri=2/33.png" alt=""></figure>
                                <div class="view-btn"><a href="assets/images/galeri=2/33.png" class="lightbox-image" data-fancybox="gallery" data-song="song32"><i class="flaticon-plus"></i></a></div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6 col-sm-12 gallery-block">
                        <div class="gallery-block-two">
                            <div class="inner-box">
                                <figure class="image"><img src="assets/images/galeri=2/34.png" alt=""></figure>
                                <div class="view-btn"><a href="assets/images/galeri=2/34.png" class="lightbox-image" data-fancybox="gallery" data-song="song33"><i class="flaticon-plus"></i></a></div>
                            </div>
                        </div>
                    </div>
                    
                </div>
            </div>
        </section>
        <!-- gallery-page-section end -->


        <!-- main-footer -->
        <footer class="main-footer pt-0">
            <div class="auto-container">
                
                <div class="widget-section">
                    <div class="row clearfix">
                        <div class="col-lg-4 col-sm-12 footer-column">
                            <div class="footer-widget links-widget">
                                <div class="widget-title">
                                    <h4>Navigasi</h4>
                                </div>
                                <div class="widget-content">
                                    <ul class="links-list clearfix">
                                        <li><a href="index.html#home">Beranda</a></li>
                                        <li><a href="index.html#tentang">Tentang</a></li>
                                        <li><a href="#galeri">Galeri</a></li>
                                        <li><a href="index.html#vitrual">Jelajah Virtual</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 col-sm-12 footer-column">
                            <div class="footer-widget contact-widget">
                                <div class="widget-title">
                                    <h4>Kontak</h4>
                                </div>
                                <div class="widget-content" style="text-align: justify;">
                                    <p>Jl. Gajah Mada No.22, Dauh Puri Kangin, Kec. Denpasar Utara, Kota Denpasar, Bali 80232</p>
                                    <ul class="social-links clearfix">
                                        <li><a href="https://api.whatsapp.com/send/?phone=6285155026771&text&type=phone_number&app_absent=0"><i class="fab fa-whatsapp"></i></a></li>
                                        <li><a href="https://www.instagram.com/ambaraa___"><i class="fab fa-instagram"></i></a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 col-sm-12 footer-column">
                            <div class="footer-widget newsletter-widget">
                                <div class="widget-title">
                                    <h4>Terima kasih</h4>
                                </div>
                                <div class="widget-content" style="text-align: justify;">
                                    <p>Terima kasih telah mengunjungi halaman web ini. Kami sangat menghargai kunjungan Anda dan harap Anda menemukan informasi yang berguna.</p>
                                    <form action="contact.html" method="post" class="newsletter-form">
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </footer>
        <!-- main-footer end -->


        <!-- scroll to top -->
        <a class="scroll-top scroll-to-target" href="#home">
            <i class="fal fa-long-arrow-up"></i>
        </a>
        <input type="hidden" id="play_song" val="">
        <audio id="song1">
            <source src="audio/1.m4a" type="audio/mp3">
        </audio>
        
        <audio id="song2">
            <source src="audio/2.m4a" type="audio/mp3">
        </audio>
        <audio id="song3">
            <source src="audio/3.m4a" type="audio/mp3">
        </audio>
        <audio id="song4">
            <source src="audio/4.m4a" type="audio/mp3">
        </audio>
        <audio id="song5">
            <source src="audio/5.m4a" type="audio/mp3">
        </audio>
        <audio id="song6">
            <source src="audio/6.m4a" type="audio/mp3">
        </audio>
        <audio id="song7">
            <source src="audio/7.m4a" type="audio/mp3">
        </audio>
        <audio id="song8">
            <source src="audio/8.m4a" type="audio/mp3">
        </audio>
        <audio id="song9">
            <source src="audio/9.m4a" type="audio/mp3">
        </audio>
        <audio id="song10">
            <source src="audio/10.m4a" type="audio/mp3">
        </audio>
        <audio id="song11">
            <source src="audio/11.m4a" type="audio/mp3">
        </audio>
        <audio id="song12">
            <source src="audio/12.m4a" type="audio/mp3">
        </audio>
        <audio id="song13">
            <source src="audio/13.m4a" type="audio/mp3">
        </audio>
        <audio id="song14">
            <source src="audio/14.m4a" type="audio/mp3">
        </audio>
        <audio id="song15">
            <source src="audio/15.m4a" type="audio/mp3">
        </audio>
        <audio id="song16">
            <source src="audio/16.m4a" type="audio/mp3">
        </audio>
        <audio id="song17">
            <source src="audio/17.m4a" type="audio/mp3">
        </audio>
        <audio id="song18">
            <source src="audio/18.m4a" type="audio/mp3">
        </audio>
        <audio id="song19">
            <source src="audio/19.m4a" type="audio/mp3">
        </audio>
        <audio id="song20">
            <source src="audio/20.m4a" type="audio/mp3">
        </audio>
        <audio id="song21">
            <source src="audio/21.m4a" type="audio/mp3">
        </audio>
        <audio id="song22">
            <source src="audio/22.m4a" type="audio/mp3">
        </audio>
        <audio id="song23">
            <source src="audio/23.m4a" type="audio/mp3">
        </audio>
        <audio id="song24">
            <source src="audio/24.m4a" type="audio/mp3">
        </audio>
        <audio id="song25">
            <source src="audio/25.m4a" type="audio/mp3">
        </audio>
        <audio id="song26">
            <source src="audio/26.m4a" type="audio/mp3">
        </audio>
        <audio id="song27">
            <source src="audio/27.m4a" type="audio/mp3">
        </audio>
        <audio id="song28">
            <source src="audio/28.m4a" type="audio/mp3">
        </audio>
        <audio id="song29">
            <source src="audio/29.m4a" type="audio/mp3">
        </audio>
        <audio id="song30">
            <source src="audio/30.m4a" type="audio/mp3">
        </audio>
        <audio id="song31">
            <source src="audio/31.m4a" type="audio/mp3">
        </audio>
        <audio id="song32">
            <source src="audio/32.m4a" type="audio/mp3">
        </audio>
        <audio id="song33">
            <source src="audio/33.m4a" type="audio/mp3">
        </audio>
    </div>


    <!-- jequery plugins -->
    <script src="assets/js/jquery.js"></script>
    <script src="assets/js/popper.min.js"></script>
    <script src="assets/js/bootstrap.min.js"></script>
    <script src="assets/js/owl.js"></script>
    <script src="assets/js/wow.js"></script>
    <script src="assets/js/validation.js"></script>
    <script src="assets/js/jquery.fancybox.js"></script>
    <script src="assets/js/appear.js"></script>
    <script src="assets/js/scrollbar.js"></script>
    <script src="assets/js/parallax-scroll.js"></script>
    <script src="assets/js/isotope.js"></script>

    <!-- main-js -->
    <script src="assets/js/script.js"></script>

</body><!-- End of .page_wrapper -->
</html>
